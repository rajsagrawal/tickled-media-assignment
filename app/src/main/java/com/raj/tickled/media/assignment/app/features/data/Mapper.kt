package com.raj.tickled.media.assignment.app.features.data

import com.raj.tickled.media.assignment.app.features.data.entities.RepoData

/**
 * Created by Raj Agrawal.
 */
fun parseAndFetchRepoList(list: List<RepoResponse>): List<RepoData>{

    val arrayList = arrayListOf<RepoData>()

    list.map {it->
        arrayList.add(RepoData(it.id, it.owner?.login, it.name, it.owner?.avatar_url, it.description))
    }

    return arrayList
}