package com.raj.tickled.media.assignment.app.features.ui

import com.raj.tickled.media.assignment.app.core.displayables.Displayable

interface Navigator {
    fun showDetailFragment(displayable: Displayable)
    fun showListFragment()
}
