package com.raj.tickled.media.assignment.app.core.states

/**
 * Created by Raj Agrawal.
 */

/**
 * Neatly wrapping responses into a container from the data source to anywhere requested.
 */
sealed class Outcome<T> {
    data class Success<T>(val response: T) : Outcome<T>()
    data class Failure<T>(val failure: Throwable) : Outcome<T>()

    companion object {
        fun <T> success(data: T): Outcome<T> = Success(data)
        fun <T> failure(type: Throwable): Outcome<T> = Failure(type)
    }
}

/**
 * Exception wrapper for 2 likely encounters - Network-side and Client-side
 */
class NetworkException(override var message: String) : Throwable(message)

class ClientException(override var message: String) : Throwable(message)
