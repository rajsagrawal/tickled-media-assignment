package com.raj.tickled.media.assignment.app.features.data

import androidx.lifecycle.MutableLiveData
import com.raj.tickled.media.assignment.app.core.states.NetworkException
import com.raj.tickled.media.assignment.app.core.states.Outcome
import com.raj.tickled.media.assignment.app.features.data.apis.GitAPI
import com.raj.tickled.media.assignment.app.features.data.entities.RepoData
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers

/**
 * Created by Raj Agrawal.
 */
class GitDataSource(private val api: GitAPI) : GitRepoContract.Remote {

    private var disposable: Disposable? = null

    override fun onGetRepoList(userName: String): MutableLiveData<Outcome<List<RepoData>>> {

        val liveData = MutableLiveData<Outcome<List<RepoData>>>()

        val resultObservable = api.getRepoList(userName)

        disposable = resultObservable
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.computation())
                .map { response ->
                    if (response.isSuccessful) {
                        response.body()?.run {
                            Outcome.success(parseAndFetchRepoList(this))
                        }
                    } else {
                        Outcome.failure(NetworkException(response.message()))
                    }
                }
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { response, failure ->
                    if (response != null) liveData.postValue(response)
                    else liveData.postValue(Outcome.failure(NetworkException(failure.message.toString())))
                }

        return liveData
    }

    override fun dispose() {
        disposable?.dispose()
    }
}