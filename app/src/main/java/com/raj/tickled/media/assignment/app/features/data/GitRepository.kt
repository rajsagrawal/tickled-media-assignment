package com.raj.tickled.media.assignment.app.features.data

import androidx.lifecycle.MutableLiveData
import com.raj.tickled.media.assignment.app.core.states.Outcome
import com.raj.tickled.media.assignment.app.features.data.entities.RepoData

/**
 * Created by Raj Agrawal.
 */
class GitRepository(private val repo: GitRepoContract.Remote): GitRepoContract.Repository {

    override fun getRepoList(userName: String): MutableLiveData<Outcome<List<RepoData>>> =
            repo.onGetRepoList(userName)

    override fun dispose() = repo.dispose()
}