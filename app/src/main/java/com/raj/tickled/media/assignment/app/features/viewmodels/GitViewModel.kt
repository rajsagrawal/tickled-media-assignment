package com.raj.tickled.media.assignment.app.features.viewmodels

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.raj.tickled.media.assignment.app.core.states.Outcome
import com.raj.tickled.media.assignment.app.features.data.GitRepoContract
import com.raj.tickled.media.assignment.app.features.data.entities.RepoData

/**
 * Created by Raj Agrawal.
 */
class GitViewModel(private val repo: GitRepoContract.Repository) : ViewModel() {

    fun getRepoList(userName: String): MutableLiveData<Outcome<List<RepoData>>> = repo.getRepoList(userName)

    override fun onCleared() {
        repo.dispose()
        super.onCleared()
    }
}