package com.raj.tickled.media.assignment.app.features.data.entities

import com.raj.tickled.media.assignment.app.core.displayables.Displayable
import kotlinx.android.parcel.Parcelize

/**
 * Created by Raj Agrawal.
 */
@Parcelize
data class RepoData(val repoID: Int? = 0,
                    val userName: String? = "user_name",
                    val repoName: String? = "repo_name",
                    val avatarImageURL: String?,
                    val description: String? = "empty") : Displayable