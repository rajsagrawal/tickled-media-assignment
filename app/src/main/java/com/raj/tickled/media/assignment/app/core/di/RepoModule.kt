package com.raj.tickled.media.assignment.app.core.di

import android.app.Application
import android.content.Context
import com.facebook.stetho.okhttp3.StethoInterceptor
import com.raj.tickled.media.assignment.BuildConfig
import com.raj.tickled.media.assignment.app.features.data.GitDataSource
import com.raj.tickled.media.assignment.app.features.data.GitRepository
import com.raj.tickled.media.assignment.app.features.data.apis.GitAPI
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.moshi.MoshiConverterFactory
import java.util.concurrent.TimeUnit
import javax.inject.Inject
import javax.inject.Singleton

/**
 * Created by Raj Agrawal.
 */
@Module()
class RepoModule(private val application: Application) {

    @Provides
    fun providesAppContext(): Context {
        return application
    }

    @Provides
    @Singleton
    fun getRetrofit(): Retrofit {

        val httpClient = OkHttpClient.Builder()

        httpClient
                .addInterceptor { chain ->
                    val request = chain.request().newBuilder().build()
                    val response = chain.proceed(request)
                    response
                }.connectTimeout(60, TimeUnit.SECONDS)
                .readTimeout(60, TimeUnit.SECONDS)

        httpClient
                .addNetworkInterceptor(StethoInterceptor())

        val builder = Retrofit.Builder()
                .baseUrl(BuildConfig.BASE_DOMAIN)
                .addConverterFactory(MoshiConverterFactory.create())
                .addCallAdapterFactory(RxJava2CallAdapterFactory.create())

        val client = httpClient.build()
        return builder.client(client).build()
    }

    @Provides
    @Inject
    fun getGitRepository(retrofit: Retrofit): GitRepository {
        return GitRepository(GitDataSource(retrofit.create(GitAPI::class.java)))
    }
}
