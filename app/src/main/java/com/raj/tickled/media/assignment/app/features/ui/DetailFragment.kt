package com.raj.tickled.media.assignment.app.features.ui

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.raj.tickled.media.assignment.R
import com.raj.tickled.media.assignment.app.core.di.AppComponent
import com.raj.tickled.media.assignment.app.core.extensions.bind
import com.raj.tickled.media.assignment.app.core.extensions.gone
import com.raj.tickled.media.assignment.app.core.extensions.loadFromUrl
import com.raj.tickled.media.assignment.app.features.data.entities.RepoData
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.layout_repo_card_content.view.*

/**
 * Created by Raj Agrawal.
 */
class DetailFragment : Fragment() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponent.from(this.requireContext())?.inject(this)
        retrieveFromBundle()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        if (isAdded && !isDetached) loadData()
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    private fun loadData() {
        progress.gone()
        repoData?.run {
            recycler_view.bind(listOf(this)).map(layout = R.layout.layout_repo_card_content, predicate = { true }) { item, _ ->
                image_avatar.loadFromUrl(item.avatarImageURL)
                text_repo_name.gone()
                text_description.text = item.description
            }
        }
    }

    private var repoData: RepoData? = null

    private fun retrieveFromBundle() {
        repoData = arguments?.getParcelable("data")
    }
}