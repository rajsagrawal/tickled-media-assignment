package com.raj.tickled.media.assignment.app.features.data.apis

import com.raj.tickled.media.assignment.BuildConfig
import com.raj.tickled.media.assignment.app.features.data.RepoResponse
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path

/**
 * Created by Raj Agrawal.
 */
interface GitAPI {
    @GET(BuildConfig.BASE_DOMAIN + REPO_LIST_PATH)
    fun getRepoList(@Path("user_name") user_name: String): Single<Response<List<RepoResponse>>>
}