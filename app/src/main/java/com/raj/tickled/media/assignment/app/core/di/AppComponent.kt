package com.raj.tickled.media.assignment.app.core.di

import android.content.Context
import com.raj.tickled.media.assignment.app.core.MainApplication
import com.raj.tickled.media.assignment.app.features.ui.DetailFragment
import com.raj.tickled.media.assignment.app.features.ui.ListFragment
import com.raj.tickled.media.assignment.app.features.ui.MainActivity
import dagger.Component
import javax.inject.Singleton

/**
 * Created by Raj Agrawal.
 */
@Component(modules = [(RepoModule::class)])
@Singleton
abstract class AppComponent {

    abstract fun inject(mainApplication: MainApplication)

    abstract fun inject(mainActivity: MainActivity)

    abstract fun inject(listFragment: ListFragment)

    abstract fun inject(detailFragment: DetailFragment)

    companion object {

        fun from(context: Context): AppComponent? {
            return (context.applicationContext as MainApplication).appComponent
        }
    }
}
