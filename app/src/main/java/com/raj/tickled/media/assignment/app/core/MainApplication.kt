package com.raj.tickled.media.assignment.app.core

import android.app.Application
import com.raj.tickled.media.assignment.app.core.di.AppComponent
import com.raj.tickled.media.assignment.app.core.di.DaggerAppComponent
import com.raj.tickled.media.assignment.app.core.di.RepoModule

/**
 * Created by Raj Agrawal.
 */
class MainApplication : Application() {

    lateinit var appComponent: AppComponent

    override fun onCreate() {
        super.onCreate()

        appComponent = DaggerAppComponent.builder()
                .repoModule(RepoModule(this))
                .build()

        appComponent.inject(this)
    }
}