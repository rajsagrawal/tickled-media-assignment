package com.raj.tickled.media.assignment.app.features.ui

import android.os.Bundle
import android.view.MenuItem
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.raj.tickled.media.assignment.BuildConfig
import com.raj.tickled.media.assignment.R
import com.raj.tickled.media.assignment.app.core.di.AppComponent
import com.raj.tickled.media.assignment.app.core.displayables.Displayable
import com.raj.tickled.media.assignment.app.features.data.entities.RepoData
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by Raj Agrawal.
 */
class MainActivity : AppCompatActivity(), Navigator {

    private val listFragment = ListFragment()
    private val detailFragment = DetailFragment()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponent.from(this)?.inject(this)
        setContentView(R.layout.activity_main)
        setToolbar()
        showListFragment()
    }

    private fun setToolbar() {
        setSupportActionBar(toolbar_default)
    }

    private fun configureToolbar(title: String, showBackButton: Boolean = false) {
        supportActionBar?.title = title
        supportActionBar?.setDisplayHomeAsUpEnabled(showBackButton)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            showListFragment()
        }
        return super.onOptionsItemSelected(item)
    }

    override fun showDetailFragment(displayable: Displayable) {
        if (displayable is RepoData) {
            configureToolbar("You're viewing " + displayable.repoName, true)

            val bundle = Bundle()
            bundle.putParcelable("data", displayable)

            val fragmentTransaction = supportFragmentManager.beginTransaction()
            detailFragment.arguments = bundle
            fragmentTransaction.hide(listFragment)
            fragmentTransaction.add(R.id.container, detailFragment, ID_DETAIL_FRAGMENT)
            fragmentTransaction.commit()
        } else {
            if (BuildConfig.DEBUG) Toast.makeText(this, "Displayable not of type " + RepoData::class.simpleName, Toast.LENGTH_LONG).show()
            else Toast.makeText(this, "Error", Toast.LENGTH_LONG).show()
        }
    }

    override fun showListFragment() {
        configureToolbar("Git repositories by OctoKit", false)

        if (supportFragmentManager.findFragmentByTag(ID_LIST_FRAGMENT) == null) {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.add(R.id.container, listFragment, ID_LIST_FRAGMENT)
            fragmentTransaction.commit()
        } else {
            val fragmentTransaction = supportFragmentManager.beginTransaction()
            fragmentTransaction.remove(detailFragment)
            fragmentTransaction.commit()
            fragmentTransaction.show(listFragment)
        }
    }

    override fun onBackPressed() {
        if (detailFragment.isAdded) showListFragment()
        else super.onBackPressed()
    }

    companion object {
        private const val ID_LIST_FRAGMENT = "listFragment"
        private const val ID_DETAIL_FRAGMENT = "detailFragment"
    }
}
