package com.raj.tickled.media.assignment.app.features.data.apis

/**
 * Created by Raj Agrawal.
 */
const val REPO_LIST_PATH = "orgs/{user_name}/repos"