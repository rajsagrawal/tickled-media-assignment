package com.raj.tickled.media.assignment.app.features.data

import androidx.lifecycle.MutableLiveData
import com.raj.tickled.media.assignment.app.core.states.Outcome
import com.raj.tickled.media.assignment.app.features.data.entities.RepoData

/**
 * Created by Raj Agrawal.
 */
class GitRepoContract {
    interface Repository {
        fun getRepoList(userName: String): MutableLiveData<Outcome<List<RepoData>>>
        fun dispose()
    }

    interface Remote {
        fun onGetRepoList(userName: String): MutableLiveData<Outcome<List<RepoData>>>
        fun dispose()
    }
}