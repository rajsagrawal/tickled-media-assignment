package com.raj.tickled.media.assignment.app.features.ui

import android.content.Context
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import com.raj.tickled.media.assignment.R
import com.raj.tickled.media.assignment.app.core.di.AppComponent
import com.raj.tickled.media.assignment.app.core.displayables.Displayable
import com.raj.tickled.media.assignment.app.core.extensions.bind
import com.raj.tickled.media.assignment.app.core.extensions.gone
import com.raj.tickled.media.assignment.app.core.states.Outcome
import com.raj.tickled.media.assignment.app.features.data.GitRepository
import com.raj.tickled.media.assignment.app.features.data.entities.RepoData
import com.raj.tickled.media.assignment.app.features.viewmodels.GitViewModel
import kotlinx.android.synthetic.main.fragment_list.*
import kotlinx.android.synthetic.main.layout_repo_card.view.*
import kotlinx.android.synthetic.main.layout_repo_card_content.view.*
import javax.inject.Inject

/**
 * Created by Raj Agrawal.
 */
class ListFragment : Fragment() {

    private var navigator: Navigator? = null

    private var clickListener: (Displayable) -> Unit = { }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        if (context is MainActivity) {
            navigator = context
        }
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        AppComponent.from(this.requireContext())?.inject(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        loadData()
        assignListeners()
    }

    private fun assignListeners() {
        clickListener = { type ->
            when (type) {
                is RepoData -> navigator?.showDetailFragment(type)
            }
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_list, container, false)
    }

    private lateinit var viewModel: GitViewModel

    @Inject
    lateinit var repo: GitRepository

    private val repoName = "octokit"

    private fun loadData() {
        viewModel = GitViewModel(repo)

        viewModel.getRepoList(repoName).observe(this, Observer { outcome ->
            when (outcome) {
                is Outcome.Success -> {
                    progress.gone()
                    outcome.response.run {
                        recycler_view.bind(this).map(R.layout.layout_repo_card, predicate = { true }) { item, _ ->
                            image_avatar.gone()
                            text_repo_name.text = item.repoName
                            text_description.gone()
                            card.setOnClickListener {
                                clickListener(item)
                            }
                        }
                    }
                }
                is Outcome.Failure -> {
                    progress.gone()
                    Toast.makeText(this@ListFragment.requireContext(), outcome.failure.message, Toast.LENGTH_LONG).show()
                }
            }
        })
    }
}