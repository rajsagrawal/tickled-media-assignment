package com.raj.tickled.media.assignment.app.core.displayables

import android.os.Parcelable

/**
 * Created by Raj Agrawal.
 */
/**
 * Marker interface to identify data-entities to consume within views.
 */
interface Displayable : Parcelable