Project as per the following requirents:

Create an Android application using Kotlin or JAVA that has simple Listing & Details view of Github repositories. Android app should have one Activity & two Fragments one for listing & one for details view. 
Use MVVM (must) as project architecture & decide which Android Architecture Components (must) to be used as applicable.

* Other instructions
* Create a GitHub repo for the assignment & share the same once the assignment is completed
* Use GitHub APIs for Listing & Details view
* Feel free to use any 3rd party libraries
* Write Loosely coupled code
* Add source code comments as required